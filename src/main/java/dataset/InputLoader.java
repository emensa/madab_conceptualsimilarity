/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataset;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import similarity.Similarity;

/**
 * Loads the input.
 * @author Mensa Enrico
 */
public class InputLoader {

    /**
     * Load word couples into {@code couple_list}.
     *
     * @return Couple input list.
     */
    public static List<Couple> loadInputCouples() {
        List<Couple> couple_list = new ArrayList<>();


        try {
            String file_name = "input_couples.txt";

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(file_name);
            InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(streamReader);
            
            for (String line; (line = reader.readLine()) != null;) {
                String[] split = line.split("\t");
                String word1 = split[0];
                String word2 = split[1];
                couple_list.add(new Couple(word1, word2));
            }


        } catch (IOException ex) {
            Logger.getLogger(Similarity.class.getName()).log(Level.SEVERE, null, ex);
        }
        return couple_list;
    }
}
