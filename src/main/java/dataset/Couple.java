/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataset;

/**
 * Represent a couple of word forms.
 *
 * @author Mensa Enrico
 */
public class Couple {

    private String word1;
    private String word2;

    Couple(String word1, String word2) {
        this.word1 = word1;
        this.word2 = word2;
    }

    public String getWord1() {
        return word1;
    }

    public void setWord1(String word1) {
        this.word1 = word1;
    }

    public String getWord2() {
        return word2;
    }

    public void setWord2(String word2) {
        this.word2 = word2;
    }

    @Override
    public String toString() {
        return "Couple (" + word1 + ", " + word2 + ") " + "\t";
    }

}
