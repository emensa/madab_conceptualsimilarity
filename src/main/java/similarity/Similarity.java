/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity;

import dataset.Couple;
import edu.smu.tspell.wordnet.NounSynset;

import java.util.List;

/**
 * Similarity functions.
 *
 * @author Mensa Enrico
 */
public class Similarity {

    /**
     * Compute similarity score for each couple.
     *
     * @param couple_list: list of input couple of words.
     */
    public static void computeAndPrintSimilarities(List<Couple> couple_list) {

        for (Couple couple : couple_list) { //For each couple
            double similarityScore = computeSimilarityForCouple(couple);
            System.out.println(couple.toString() + " scores " + similarityScore);
        }
    }

    /**
     * Compute similarity score for input couple
     *
     * !!!! JAWS docs:
     * http://web.archive.org/web/20160516013111/http://lyle.smu.edu:80/~tspell/jaws/doc/edu/smu/tspell/wordnet/package-summary.html
     *
     * @param couple: couple
     * @return similarity score for {@code couple}.
     */
    private static double computeSimilarityForCouple(Couple couple) {

        //TODO:
        //1. Get synsets for each word of the couple
        //2. Compute WU Palmer similarity for each combination
        //3. Get max similarity
        //4. Return similarity
        return 0.0;
    }

    /**
     * Compute Wu Palmer's Conceptual Similarity;
     *
     * Wu Palmer's Conceptual similarity score is computed as score(s1,s2) =
     * 2*depth(LCS)/(depth(s1)+depth(s2)); where depth(s1) is the distance
     * between the root and the s1 and LCS (Least Common Superconcept) is the
     * most specific concept which is an ancestor of both s1 and s2.
     *
     * @param s1: first WordNet Synset
     * @param s2: second WordNet Synset
     *
     * @return conceptual similarity score.
     */
    public static double wuPalmer(NounSynset s1, NounSynset s2) {
        //TODO
    	return 0.0;
    }
}


