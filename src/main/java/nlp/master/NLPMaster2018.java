/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nlp.master;

import dataset.Couple;
import dataset.InputLoader;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import java.io.File;
import java.util.List;
import similarity.Similarity;

/**
 * NLP Master for MADAB.
 * @author Mensa Enrico
 */
public class NLPMaster2018 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setProperty("wordnet.database.dir", "." + File.separator + "lib" + File.separator + "WordNet-3.0" + File.separator + "dict" + File.separator);

        //Get some synsets to test WordNet connection: delete me
        testWordNet();

        List<Couple> inputCouples = InputLoader.loadInputCouples();

        Similarity.computeAndPrintSimilarities(inputCouples);
    }

    private static void testWordNet() {
        NounSynset nounSynset;
        NounSynset[] hypernyms;

        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets("fly", SynsetType.NOUN);
        for (Synset synset : synsets) {
            nounSynset = (NounSynset) (synset);
            hypernyms = nounSynset.getHypernyms();
            System.out.println(nounSynset.getWordForms()[0] + ": "
                    + nounSynset.getDefinition() + ") has "
                    + hypernyms.length + " hypernyms");
        }
    }
}
